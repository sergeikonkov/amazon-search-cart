Run test:
 - mvn clean test

Test results:
- mvn allure:serve

Technologies:
java, selenide, allure, maven, testNG, owner aeonbits properties

Test path:
 - src/test/java/amazon.cart/CheckCartTest.java

Pages path:
 - src/main/java/core/pages

 Useful links:
  - https://selenide.org/