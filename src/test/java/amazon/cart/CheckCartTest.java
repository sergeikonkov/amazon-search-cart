package amazon.cart;

import amazon.core.BaseTest;
import org.testng.annotations.Test;

public class CheckCartTest extends BaseTest {
    @Test()
    public void userShouldBeAbleToConvertCurrency() {
        // --------------------- Test Data ----------------------//
        String BOOK_EXPERIENCE_OF_TA = "Experiences of Test Automation: Case Studies of Software Test Automation";
        String BOOK_AGILE_TESTING = "Agile Testing: A Practical Guide for Testers and Agile Teams";
        String BOOK_SELENIUM = "Selenium WebDriver 3 Practical Guide: End-to-end automation testing for web and mobile browsers with Selenium WebDriver, 2nd Edition";

        // --------------------- Test Case ----------------------//

        homePage().openPage()
                .typeIntoSearchInput(BOOK_EXPERIENCE_OF_TA)
                .clickSearchButton()

                .checkResultsAreaDisplayed()
                .checkFirstResultTitleIsEqualTo(BOOK_EXPERIENCE_OF_TA)
                .clickFirstResultTitleIsEqualTo();

        productDetailPageWithMultipleChoice()
                .selectPaperMediaType()
                .clickAddToCartButton()

                .checkCartSubtotalAreaDisplayed();


        homePage().openPage()
                .typeIntoSearchInput(BOOK_AGILE_TESTING)
                .clickSearchButton()

                .checkResultsAreaDisplayed()
                .checkFirstResultTitleIsEqualTo(BOOK_AGILE_TESTING)
                .clickFirstResultTitleIsEqualTo();

        productDetailPageWithMultipleChoice()
                .clickAddToCartButton()

                .checkCartSubtotalAreaDisplayed();

        homePage().openPage()
                .typeIntoSearchInput(BOOK_SELENIUM)
                .clickSearchButton()

                .checkResultsAreaDisplayed()
                .checkFirstResultTitleIsEqualTo(BOOK_SELENIUM)
                .clickFirstResultTitleIsEqualTo();

        productDetailPage()
                .selectPaperMediaType()
                .clickAddToCartButton()

                .checkCartSubtotalAreaDisplayed();

        cartPage()
                .openPage()
                .clickSaveForLaterLinkForCartItemWithTitle(BOOK_EXPERIENCE_OF_TA)
                .checkCartItemWithTitleDisplayedInSavedForLaterList(BOOK_EXPERIENCE_OF_TA)

                .clickDeleteItemWithTitle(BOOK_AGILE_TESTING)
                .checkCartItemNotDisplayed(BOOK_AGILE_TESTING)

                .selectQuantityForCartItemWithTitle(BOOK_SELENIUM, "3")

                .selectAsGiftForCartItemWithTitle(BOOK_SELENIUM)

                .clickDeleteItemWithTitle(BOOK_SELENIUM)
                .checkCartItemNotDisplayed(BOOK_SELENIUM);
    }
}
