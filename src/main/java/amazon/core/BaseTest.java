package amazon.core;

import amazon.core.internal.GenericPage;
import amazon.core.internal.PageObjectsSupplier;
import amazon.core.internal.testcases.TestCase;
import amazon.core.listener.AllureReportListener;
import amazon.core.listener.EventListener;
import amazon.core.listener.Highlighter;
import com.codeborne.selenide.WebDriverRunner;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import java.util.HashMap;
import java.util.Map;

import static amazon.utils.AttachmentUtils.attachLog;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.WebDriverRunner.addListener;
import static org.apache.commons.collections.MapUtils.isEmpty;

@Listeners({
        AllureReportListener.class,
})
public abstract class BaseTest implements PageObjectsSupplier, TestCase {

    private static final Logger LOGGER = Logger.getLogger(BaseTest.class);

    @BeforeSuite(alwaysRun = true)
    public void setUp() {
        addListener(new EventListener());
        addListener(new Highlighter());
    }

    /**
     * Retrieves the user agent from the browser
     * @return - String of the user agent
     */
    public void userAgent() {
        String ua;
        try {
            ua = executeJavaScript("return navigator.userAgent;");
        } catch (Exception e) {
            ua = "Unable to fetch UserAgent";
        }
        LOGGER.debug("User agent is: '" + ua + "'");
        attachLog("User agent", ua);
    }

    /**
     * Returns the webdriver object for that given thread
     * @return - WebDriver object
     */
    public static WebDriver getDriver() {
        return WebDriverRunner.getWebDriver();
    }

    /**
     * Thread-safe container in which are stored values as page instances
     */
    private static final ThreadLocal<Map<GenericPage, BasePage>> PAGES = ThreadLocal.withInitial(HashMap::new);

    public static Map<GenericPage, BasePage> getPages() {
        return PAGES.get();
    }

    /**
     * Cleaning collection Pages
     */
    private void cleanUpPages() {
        if (!isEmpty(getPages())) {
            PAGES.remove();
        }
    }
}



