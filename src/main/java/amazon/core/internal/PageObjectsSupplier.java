package amazon.core.internal;

import amazon.core.BasePage;
import amazon.core.pages.CartPage;
import amazon.core.pages.ProductBookDetailPage;
import amazon.core.pages.ProductBookDetailWithMultipleBuyingChoicePage;
import amazon.core.pages.HomePage;

import static amazon.core.internal.GenericPage.getPageObject;
import static amazon.core.internal.PageObjectsSupplier.PageObject.*;

public interface PageObjectsSupplier<T extends PageObjectsSupplier<T>> {

    enum PageObject implements GenericPage {

        HOME_PAGE {
            public BasePage create() {
                return new HomePage();
            }
        },

        PRODUCT_DETAIL_PAGE {
            public BasePage create() {
                return new ProductBookDetailPage();
            }
        },

        PRODUCT_DETAIL_WITH_MULTIPLE_CHOICE_PAGE {
            public BasePage create() {
                return new ProductBookDetailWithMultipleBuyingChoicePage();
            }
        },
        CART_PAGE {
            public BasePage create() {return new CartPage(); }
        }
    }

    default HomePage homePage() {
        return (HomePage) getPageObject(HOME_PAGE);
    }

    default ProductBookDetailPage productDetailPage() {
        return (ProductBookDetailPage) getPageObject(PRODUCT_DETAIL_PAGE);
    }

    default ProductBookDetailWithMultipleBuyingChoicePage productDetailPageWithMultipleChoice() {
        return (ProductBookDetailWithMultipleBuyingChoicePage) getPageObject(PRODUCT_DETAIL_WITH_MULTIPLE_CHOICE_PAGE);
    }

    default CartPage cartPage() {
        return (CartPage) getPageObject(CART_PAGE);
    }

}
