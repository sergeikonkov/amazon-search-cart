package amazon.core.internal;

import amazon.core.BaseTest;
import amazon.core.BasePage;

public interface GenericPage {

    static BasePage getPageObject(final GenericPage page) {
        BaseTest.getPages().putIfAbsent(page, page.create());
        return BaseTest.getPages().get(page);
    }

    BasePage create();
}
