package amazon.core.pages;

import amazon.core.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class ProductBookDetailPage extends BasePage<ProductBookDetailPage> {
    private static final By ADD_TO_CART_BUTTON = By.id("add-to-cart-button");
    private static final By PAPERBACK_TAB = By.cssSelector("a[href *='pap_swatch']");

    @Override
    public String getUrl() {
        return baseUrl;
    }

    @Step("Select \"Paper\" media type")
    public ProductBookDetailPage selectPaperMediaType() {
        $(PAPERBACK_TAB).click();
        return this;
    }

    @Step("Click \"Add to cart\" button")
    public CartSubtotalPage clickAddToCartButton() {
        $(ADD_TO_CART_BUTTON).click();
        return page(CartSubtotalPage.class);
    }
}
