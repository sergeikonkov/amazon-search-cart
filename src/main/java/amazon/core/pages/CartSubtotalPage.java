package amazon.core.pages;

import amazon.core.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class CartSubtotalPage extends BasePage<CartSubtotalPage> {
    private static final By CART_SUBTOTAL_AREA = By.id("huc-page-container");

    @Override
    public String getUrl() {
        return baseUrl;
    }

    @Step("Check Cart subtotal area displayed")
    public CartSubtotalPage checkCartSubtotalAreaDisplayed() {
        $(CART_SUBTOTAL_AREA).shouldBe(visible);
        return this;
    }
}
