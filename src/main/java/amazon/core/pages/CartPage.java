package amazon.core.pages;

import com.codeborne.selenide.Condition;
import amazon.core.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;

import static com.codeborne.selenide.Selenide.$$;

public class CartPage extends BasePage<CartPage> {
    private static final By CART_ITEMS = By.cssSelector(".sc-list-body .sc-list-item-content");
    private static final By SAVE_FOR_LATER_LINK = By.cssSelector("input[name*='submit.save-for-later']");
    private static final By DELETE_LINK = By.cssSelector("input[name*='submit.delete']");
    private static final By QUANTITY_SELECT = By.cssSelector("select[name='quantity']");
    private static final By GIFT_OPTION = By.cssSelector(".sc-gift-option input");

    private static final By SAVED_FOR_LATER_CART = By.cssSelector("#sc-saved-cart .sc-list-item-content");

    @Override
    public String getUrl() {
        return baseUrl + "gp/cart/view.html";
    }

    @Step("Click \"Save for later\" link for item with title \"{title}\"")
    public CartPage clickSaveForLaterLinkForCartItemWithTitle(String title) {
        $$(CART_ITEMS).find(Condition.text(title)).$(SAVE_FOR_LATER_LINK).click();
        return this;
    }

    @Step("Click \"Delete\" link for item with title \"{title}\"")
    public CartPage clickDeleteItemWithTitle(String title) {
        $$(CART_ITEMS).find(Condition.text(title)).$(DELETE_LINK).click();
        return this;
    }

    @Step("Check item with title \"{title}\" NOT displayed in cart")
    public CartPage checkCartItemNotDisplayed(String title) {
        $$(CART_ITEMS).find(Condition.text(title)).shouldBe(hidden);
        return this;
    }

    @Step("Select \"{value}\" for item with title \"{title}\"")
    public CartPage selectQuantityForCartItemWithTitle(String title, String value) {
        $$(CART_ITEMS).find(Condition.text(title)).$(QUANTITY_SELECT).selectOptionByValue(value);
        return this;
    }

    @Step("Click \"Gift\" checkbox for item with title \"{title}\"")
    public CartPage selectAsGiftForCartItemWithTitle(String title) {
        $$(CART_ITEMS).find(Condition.text(title)).$(GIFT_OPTION).click();
        return this;
    }

    @Step("Check item with title \"{title}\" displayed in \"Saved for later\" list")
    public CartPage checkCartItemWithTitleDisplayedInSavedForLaterList(String title) {
        $$(CART_ITEMS).find(Condition.text(title)).shouldBe(visible);
        return this;
    }
}
