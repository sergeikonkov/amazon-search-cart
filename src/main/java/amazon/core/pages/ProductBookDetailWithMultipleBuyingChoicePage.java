package amazon.core.pages;

import com.codeborne.selenide.Selenide;
import amazon.core.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ProductBookDetailWithMultipleBuyingChoicePage extends BasePage<ProductBookDetailWithMultipleBuyingChoicePage> {
    private static final By ADD_TO_CART_BUTTON = By.id("add-to-cart-button");
    private static final By PAPERBACK_TAB = By.cssSelector("a[href*='mt_paperback']");

    @Override
    public String getUrl() {
        return baseUrl;
    }

    @Step("Select \"Paper\" media type")
    public ProductBookDetailWithMultipleBuyingChoicePage selectPaperMediaType() {
        $(PAPERBACK_TAB).click();
        return this;
    }

    @Step("Click \"Add to cart\" button")
    public CartSubtotalPage clickAddToCartButton() {
        $(ADD_TO_CART_BUTTON).click();
        return Selenide.page(CartSubtotalPage.class);
    }
}
