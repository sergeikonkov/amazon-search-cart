package amazon.core.pages;

import amazon.core.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;
import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.id;


public class HomePage extends BasePage<HomePage> {

    private static final By SEARCH_INPUT = id("twotabsearchtextbox");
    private static final By SEARCH_BUTTON = cssSelector(".nav-search-submit");

    @Override
    public String getUrl() {
        return baseUrl;
    }

    @Step("Type: \"{searchQuery}\" in search input")
    public HomePage typeIntoSearchInput(String searchQuery) {
        $(SEARCH_INPUT).setValue(searchQuery);
        return this;
    }

    @Step("Click search button")
    public ResultsPage clickSearchButton() {
        $(SEARCH_BUTTON).click();
        return page(ResultsPage.class);
    }
}
