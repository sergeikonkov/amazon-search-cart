package amazon.core.pages;

import amazon.core.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.page;

public class ResultsPage extends BasePage<ResultsPage> {
    private static final By RESULTS_AREA = By.id("atfResults");
    private static final By RESULTS_LIST = By.cssSelector("#s-results-list-atf li");
    private static final By RESULT_TITLE = By.cssSelector(".s-access-detail-page h2");


    @Override
    public String getUrl() {
        return baseUrl;
    }

    @Step("Results area displayed")
    public ResultsPage checkResultsAreaDisplayed() {
        $(RESULTS_AREA).shouldBe(visible);
        return this;
    }

    @Step("Check first result title is equal to \"{value}\"")
    public ResultsPage checkFirstResultTitleIsEqualTo(String value) {
        $$(RESULTS_LIST).get(0).$(RESULT_TITLE).shouldHave(text(value));
        return this;
    }

    @Step("Click first result title is equal to \"{value}\"")
    public ProductBookDetailWithMultipleBuyingChoicePage clickFirstResultTitleIsEqualTo() {
        $$(RESULTS_LIST).get(0).$(RESULT_TITLE).click();
        return page(ProductBookDetailWithMultipleBuyingChoicePage.class);
    }
}
